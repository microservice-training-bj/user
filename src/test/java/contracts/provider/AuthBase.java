package contracts.provider;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.tw.microservice.trainning.bj.Application;
import com.tw.microservice.trainning.bj.domain.AuthService;
import com.tw.microservice.trainning.bj.infrastructure.Privilege;
import com.tw.microservice.trainning.bj.infrastructure.Role;
import com.tw.microservice.trainning.bj.infrastructure.User;
import com.tw.microservice.trainning.bj.infrastructure.UserRepository;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public abstract class AuthBase {
    @Autowired
    private WebApplicationContext context;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private AuthService authService;

    @Before
    public void setup() {
        RestAssuredMockMvc.webAppContextSetup(context);

        Privilege privilege = new Privilege();
        privilege.setSymbol(Privilege.Symbol.RETRIEVE_USER);

        Role role = new Role();
        role.setSymbol(Role.Symbol.STAFF);
        role.setPrivileges(Collections.singletonList(privilege));

        User user = new User();
        user.setName("staff");
        user.setRole(role);

        doNothing().when(authService).verifyLoginInfo(any());
        given(userRepository.findByName(anyString())).willReturn(user);
    }
}
