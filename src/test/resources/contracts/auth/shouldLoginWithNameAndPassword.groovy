package auth

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'POST'
        url '/api/login'
        body("""
            {
                "name": "staff",
                "password": "123"
            }
        """)
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 200
        body("""
            {
                "username": "staff",
                "role": "STAFF",
                "privileges":["RETRIEVE_USER"]
            }
        """)
    }
}
