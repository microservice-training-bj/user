INSERT INTO `t_role` (`symbol`, `name`) VALUES ('STAFF', '门店店员');
INSERT INTO `t_role` (`symbol`, `name`) VALUES ('OPS', '营运');
INSERT INTO `t_role` (`symbol`, `name`) VALUES ('FINANCE', '财务');
INSERT INTO `t_role` (`symbol`, `name`) VALUES ('CUSTOMER_SERVICE', '客服');
INSERT INTO `t_role` (`symbol`, `name`) VALUES ('MANAGER', '经理');
INSERT INTO `t_role` (`symbol`, `name`) VALUES ('CUSTOMER', '客户');

INSERT INTO `t_user` (`id`, `created_date`, `updated_date`, `name`, `password`, role) VALUES
  ('40b2dd57ca074dc0bd757c3e36fb2ffd', 1000, 1000, 'staff',
   '$2a$10$W3rO9JXdqvH01bYK/QRYCO7VYNsPDI2tK.4zzmAarYYlE1fLoXxfe', 'STAFF');

INSERT INTO `t_user` (`id`, `created_date`, `updated_date`, `name`, `password`, role) VALUES
  ('40b2dd57ca074dc0bdjk8c3e36fb2ffd', 1000, 1000, 'manager',
   '$2a$10$W3rO9JXdqvH01bYK/QRYCO7VYNsPDI2tK.4zzmAarYYlE1fLoXxfe', 'MANAGER');

INSERT INTO `t_user` (`id`, `created_date`, `updated_date`, `name`, `password`, role) VALUES
  ('40b2dd57ca074dc123jk8c3e36fb2ffd', 1000, 1000, 'customer',
   '$2a$10$W3rO9JXdqvH01bYK/QRYCO7VYNsPDI2tK.4zzmAarYYlE1fLoXxfe', 'CUSTOMER');


INSERT INTO `t_privilege` (`symbol`, `name`) VALUES ('CREATE_USER', '创建用户');
INSERT INTO `t_privilege` (`symbol`, `name`) VALUES ('UPDATE_USER', '更新用户');
INSERT INTO `t_privilege` (`symbol`, `name`) VALUES ('RETRIEVE_USER', '检索用户');
INSERT INTO `t_privilege` (`symbol`, `name`) VALUES ('DELETE_USER', '删除用户');


INSERT INTO `t_role_privilege` (`role_symbol`, `privilege_symbol`) VALUES ('MANAGER', 'CREATE_USER');
INSERT INTO `t_role_privilege` (`role_symbol`, `privilege_symbol`) VALUES ('MANAGER', 'UPDATE_USER');
INSERT INTO `t_role_privilege` (`role_symbol`, `privilege_symbol`) VALUES ('MANAGER', 'RETRIEVE_USER');
INSERT INTO `t_role_privilege` (`role_symbol`, `privilege_symbol`) VALUES ('MANAGER', 'DELETE_USER');
INSERT INTO `t_role_privilege` (`role_symbol`, `privilege_symbol`) VALUES ('STAFF', 'RETRIEVE_USER');
