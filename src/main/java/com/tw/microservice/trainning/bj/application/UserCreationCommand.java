package com.tw.microservice.trainning.bj.application;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCreationCommand {
    private String name;
    private String password;
}
