package com.tw.microservice.trainning.bj.application.exception;

public class UserExistedException extends RuntimeException {

    public UserExistedException(String s) {
        super(s);
    }
}
