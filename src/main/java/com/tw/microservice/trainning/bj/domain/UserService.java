package com.tw.microservice.trainning.bj.domain;

import com.tw.microservice.trainning.bj.application.exception.UserExistedException;
import com.tw.microservice.trainning.bj.application.exception.UserNotExistException;
import com.tw.microservice.trainning.bj.configuration.JWTUser;
import com.tw.microservice.trainning.bj.infrastructure.Privilege;
import com.tw.microservice.trainning.bj.infrastructure.Role;
import com.tw.microservice.trainning.bj.infrastructure.User;
import com.tw.microservice.trainning.bj.infrastructure.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByName(username);
        if (user == null) {
            throw new UsernameNotFoundException("Username does not exist.");
        }
        Role role = user.getRole();
        List<Privilege.Symbol> privileges = role.getPrivileges().stream().map(Privilege::getSymbol).collect(toList());

        return JWTUser.builder().username(user.getName()).
                password(user.getPassword()).role(role.getSymbol().name()).privileges(privileges).build();
    }

    public User create(User user) throws Exception {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        if (userRepository.findByName(user.getName()) != null) {
            throw new UserExistedException("User already exists.");
        }
        return userRepository.save(user);
    }

    public User findByName(String username) {
        User user = userRepository.findByName(username);
        if (user == null) {
            throw new UserNotExistException(username);
        }
        return user;
    }
}
