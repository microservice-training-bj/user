package com.tw.microservice.trainning.bj.representation;

import com.tw.microservice.trainning.bj.application.ApplicationService;
import com.tw.microservice.trainning.bj.application.UserCreationCommand;
import com.tw.microservice.trainning.bj.infrastructure.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api")
public class CustomerController {

    @Autowired
    private ApplicationService applicationService;

    @PostMapping("/customers")
    @ResponseStatus(HttpStatus.OK)
    public User createUser(@RequestBody UserCreationCommand userCreationCommand) {
        return applicationService.createCustomer(userCreationCommand);
    }
}
