package com.tw.microservice.trainning.bj.representation;


import com.tw.microservice.trainning.bj.application.ErrorResponse;
import com.tw.microservice.trainning.bj.application.exception.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class GlobalControllerAdvice {

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(InvalidCredentialException.class)
    public void handleHackerException() {
        log.error("Invalid credential.");
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(InsuffcientPermissionException.class)
    public ErrorResponse handleInsufficientPermissionException() {
        return new ErrorResponse("Insufficient permission");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(WrongRoleException.class)
    public ErrorResponse handleWrongRoleException() {
        return new ErrorResponse("Wrong role type");
    }

    @ExceptionHandler(UserExistedException.class)
    public ResponseEntity<?> handleUserExistedException(UserExistedException e) {
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler(UserNotExistException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleUserNotExistException(UserNotExistException e) {
        log.error(e.getMessage());
    }
}
