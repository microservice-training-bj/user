package com.tw.microservice.trainning.bj.infrastructure;

import com.tw.microservice.trainning.bj.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Entity
@Table(name = "t_user")
@EntityListeners(AuditingEntityListener.class)
public class User implements Serializable {

    @Id
    private String id;

    @Column(unique = true)
    private String name;

    private String password;

    private String realName;

    @Enumerated(EnumType.STRING)
    private Department department;

    @CreatedDate
    private long createdDate;

    @LastModifiedDate
    private long updatedDate;

    @OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinColumn(name = "role")
    private Role role;

    public User() {
        id = StringUtils.uuid();
    }
}
