package com.tw.microservice.trainning.bj.application.exception;

public class UserNotExistException extends RuntimeException {
    public UserNotExistException(String username) {
        super(username + " does not exist.");
    }
}
